package bk

import (
	"io"
	"io/ioutil"
	"log"
	"os"
	"path"
	"strings"
)

func getFirstAvailableBackupName(backupDir, original string) (string, error) {
	original = strings.TrimRight(original, "/")
	backupName := original
	if backupDir != "" {
		_, err := os.Stat(backupDir)
		if os.IsNotExist(err) {
			if err := os.MkdirAll(backupDir, 0755); err != nil {
				return "", err
			}
		} else if err != nil {
			return "", err
		}

		backupName = strings.TrimRight(backupDir, "/") + "/" + strings.TrimLeft(original, "/")
	}
	backupName = backupName + ".bk"
	for {
		_, err := os.Stat(backupName)
		if os.IsNotExist(err) {
			return backupName, nil
		}
		if err != nil {
			log.Fatal(err)
		}
		backupName = backupName + ".bk"
	}
}

func copyFile(src, dst string) error {
	var err error
	var srcfd *os.File
	var dstfd *os.File
	var srcinfo os.FileInfo

	if srcfd, err = os.Open(src); err != nil {
		return err
	}
	defer srcfd.Close()

	if dstfd, err = os.Create(dst); err != nil {
		return err
	}
	defer dstfd.Close()

	if _, err = io.Copy(dstfd, srcfd); err != nil {
		return err
	}
	if srcinfo, err = os.Stat(src); err != nil {
		return err
	}
	return os.Chmod(dst, srcinfo.Mode())
}


func copyDir(src string, dst string) error {
	var err error
	var fds []os.FileInfo
	var srcinfo os.FileInfo

	if srcinfo, err = os.Stat(src); err != nil {
		return err
	}

	if err = os.MkdirAll(dst, srcinfo.Mode()); err != nil {
		return err
	}

	if fds, err = ioutil.ReadDir(src); err != nil {
		return err
	}
	for _, fd := range fds {
		srcfp := path.Join(src, fd.Name())
		dstfp := path.Join(dst, fd.Name())

		if fd.IsDir() {
			if err = copyDir(srcfp, dstfp); err != nil {
				log.Fatal(err)
			}
		} else {
			if err = copyFile(srcfp, dstfp); err != nil {
				log.Fatal(err)
			}
		}
	}
	return nil
}

func Execute(backupDir string, originals []string) error {
	for _, original := range originals {
		info, err := os.Stat(original)
		if err != nil {
			return err
		}
		backupName, err := getFirstAvailableBackupName(backupDir, strings.TrimRight(original, "/"))
		if err != nil {
			return err
		}
		if info.IsDir() {
			copyDir(original, backupName)
		} else if info.Mode().IsRegular() {
			copyFile(original, backupName)
		}
	}

	return nil
}
