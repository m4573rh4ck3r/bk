# BACKUP

A command line tool for creating a backup of the specified file with the bk extension.
It keeps adding .bk if that file already exists
until it found a file name that doesn't already exist and finally creates a backup.
