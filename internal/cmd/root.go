package cmd

import (
	"log"

	"github.com/spf13/cobra"

	"gitlab.com/m4573rh4ck3r/bk/internal/version"
	"gitlab.com/m4573rh4ck3r/bk/pkg/bk"
)

var (
	backupDir string
)

func init() {
	rootCmd.Flags().StringVarP(&backupDir, "directory", "d", "", "directory to create backup in")
	cobra.MarkFlagDirname(rootCmd.Flags(), "directory")
}

var rootCmd = &cobra.Command{
	Use:     "bk",
	Short:   "create a bk file with the .bk extension",
	Version: version.Version,
	Args:    cobra.MinimumNArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		if err := bk.Execute(backupDir, args); err != nil {
			log.Fatal(err)
		}
	},
}

func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}
